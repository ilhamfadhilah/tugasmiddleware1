<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TestController extends Controller
{
    // cara pertama memasang middleware
    // public function __construct()
    // {
    //     $this->middleware('dateMiddleware');
    // }

    public function test(){
        return 'berhasil masuk';
    }
    public function test1(){
        return 'berhasil masuk test 1';
    }

    public function admin(){
        // dd(Auth::user());
        return 'Admin berhasil masuk';
    }
    public function superadmin(){
        // dd(Auth::user());
        return 'Super Admin berhasil masuk';
    }


}
