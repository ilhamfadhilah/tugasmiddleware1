<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;

class DateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $current_data = Carbon::now()->day;
        if($current_data >= 1 && $current_data <= 20){
            return $next($request);
        }
        abort(403);
        // return'gagal masuk';

    }
}
