<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\TestController;

Route::get('/', function () {
    return view('welcome');
});


// Route::get('/test1', 'TestController@test');

// cara kedua masang middle ware
// Route::get('/test', 'TestController@test')->middleware('dateMiddleware');


//cara ketiga

Route::middleware('dateMiddleware')->group(function(){
    Route::get('/test', 'TestController@test');


});

Route::get('/test1', 'TestController@test');

Route::middleware(['auth','admin'])->group(function(){
    Route::get('/route1','TestController@superadmin');
});

Route::middleware(['auth','admin'])->group(function(){
    Route::get('/route2','TestController@admin');
});




Auth::routes();

Route::get('/route3', 'HomeController@index')->name('home');
